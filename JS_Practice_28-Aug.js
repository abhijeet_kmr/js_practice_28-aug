const data = [
  {
    place: "Melbourne",
    country: "Australia",
    location: {
      lat: "99",
      lng: "88",
    },
    temperature: "38 Degree Celsius",
  },
  {
    place: "New Delhi",
    country: "India",
    location: {
      lat: "84",
      lng: "44",
    },
    temperature: "42 Degree Celsius",
  },
  {
    place: "Pretoria",
    country: "SouthAfrica",
    location: {
      lat: "35",
      lng: "24",
    },
    temperature: "39 Degree Celsius",
  },
  {
    place: "Mexico City",
    country: "Mexico",
    location: {
      lat: "34",
      lng: "38",
    },
    temperature: "43 Degree Celsius",
  },
  {
    place: "London",
    country: "England",
    location: {
      lat: "57",
      lng: "34",
    },
    temperature: "26 Degree Celsius",
  },
];

// Note: Make sure not to mutate the array in all of the following questions.

// Q1. Get all latitude and longitude of all the places in the following manner.
// [{place: (lat, long)}, ...]

// Ans1..
const answer1 = data.reduce((acc, curr) => {
  let lat = curr.location.lat;
  let long = curr.location.lng;
  let obj = {};
  obj[curr.place] = `(${lat}, ${long})`;
  acc.push(obj);
  return acc;
}, []);
console.log(answer1);

// Q2. Sort data based on temperature (Low temperature first).
// Ans2..
let newObjForAns2 = { ...data };
const newDataOfAns2 = Object.values(newObjForAns2).sort(
  (a, b) => +a.temperature.split(" ")[0] - +b.temperature.split(" ")[0]
);
console.log(newDataOfAns2);

// Q3.Rearrange data in the following format
// [{ country: { place: { location: {lat, lng }, temperature }}}, ...]

// Ans3..
const answer3 = data.reduce((acc, curr) => {
  let country1 = curr.country;
  let place1 = curr.place;
  let temperature = curr.temperature;
  let lat = curr.location.lat;
  let lng = curr.location.lng;
  let obj = {
    [country1]: { [place1]: { location: { lat, lng }, temperature } },
  };
  acc.push(obj);
  return acc;
}, []);
console.log(...answer3);

// Q4. Change temperature of SouthAfrica "Pretoria" to "49 Degree Celsius".
// Ans4..
const changeTemperture = (targetCountry, targetPlace, newTemperature) => {
  const answer4 = data.reduce((acc, curr) => {
    const { place, country, location, temperature } = curr;
    let obj = {
      place: place,
      country: country,
      location: location,
      temperature: temperature,
    };
    if (obj.country == targetCountry && obj.place == targetPlace) {
      obj.temperature = newTemperature + " Degree Celsius";
    }
    acc.push(obj);
    return acc;
  }, []);
  console.log(answer4);
};
changeTemperture("SouthAfrica", "Pretoria", "49");

// Q5. Add a new Object in the fourth postiion.
// {
//         place: "Bangalore",
//         country: "India",
//         location: {
//             lat: '84',
//             lng: '47'
//         },
//         temperature: '29 Degree Celsius'
//  }

// Ans5..
const newObject = {
  place: "Bangalore",
  country: "India",
  location: {
    lat: "84",
    lng: "47",
  },
  temperature: "29 Degree Celsius",
};
const answer5 = data.reduce((acc, curr, index) => {
  if (index === 3) {
    acc.push(newObject);
  }
  acc.push(curr);
  return acc;
}, []);
console.log(answer5);

//  Q6. Delete the third element in the array .
// Ans6..
const answer6 = data.reduce((acc, curr, index) => {
  if (index !== 2) {
    acc.push(curr);
  }
  return acc;
}, []);
console.log(answer6);

//  Q7. Swap elements at position 2 and second last.
// Ans7..
let posTwo = data[1];
let posSecondLast = data[data.length - 2];
const answer7 = data.reduce((acc, curr, index) => {
  if (index === 1) {
    acc.push(posSecondLast);
  } else if (index === 3) {
    acc.push(posTwo);
  } else acc.push(curr);
  return acc;
}, []);
console.log(answer7);
